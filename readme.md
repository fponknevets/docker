readme.md

# Docker Deep Dive

## Module 3: Installing Docker

### Play With Docker - didn't load for me

PWD is a Docker playground which allows users to run Docker commands in a matter of seconds. It gives the experience of having a free Alpine Linux Virtual Machine in browser, where you can build and run Docker containers and even create clusters in Docker Swarm Mode. Under the hood Docker-in-Docker (DinD) is used to give the effect of multiple VMs/PCs. In addition to the playground, PWD also includes a training site composed of a large set of Docker labs and quizzes from beginner to advanced level available at training.play-with-docker.com.

### Install on Linux

`wget -qO- https://get.docker.com/ | sh`

Install automatically adds the `docker` group so you shoudl not have to run

`sudo groupadd docker`

but you will need to add your user name to the group if you do not want to have to continually use `sudo` with every Docker command 

`sudo usermod -aG docker $USER`


---
---

## Module 5

### Images

+ **image** a read-only template for creating containers
    - a bunch of files and a (JSON) manifest file
    - consists of layers
    - stored in a registry but pulled down locally using `docker image pull ...`
 + **container** a running image
    - each container has a thin writable layer


Layers live in `/var/lib/docker/overlay2/diff` if you are using the default storage driver `overlay2`.

### Registries

Where images live. In registries official images have a top level name such as:

`docker.io/redis` or

`docker.io/nginx`

Unoffical images live under a user name, such as:

`docker.io/nigelpoulton/tu-demo`

You can pull images using a command, such as:

`docker image pull redis`

However, `redis` is not the image name. When you do not specify the
+ registry name, Docker assume `docker.io` (Docker Hub)
+ image name,  Docker assumes the `latest`

So

`docker image pull redis`

is equivalent to

`docker image pull docker.io/redis:latest`

---

#### Anatomy of the registry address

|REGISTRY|REPO|IMAGE (TAG)|
|--------|----|-----------|
|`docker.io`|`redis`||
|`docker.io`|`redis`|`latest`|
|`docker.io`|`nginx`||
|`docker.io`|`nginx`|`1.13.5`|

---
---
## Module 6 Containerizing the App

### Dockerfile

An example Dockerfile

```dockerfile
# pull the base image - alpine is a lean linix image (create layer 1)
FROM alpine

# set some meta data (does not create a layer)
LABEL maintainer="steven.knopf@gmail.com"

# install nodejs and npm - apk is the alpine equivalent of apt (layer 2)
RUN apk add --update nodejs nodejs-npm

# copy all files from the cwd on the host to the /src directory in the image (layer 3)
COPY . /src

# set the images /src directory as the working directory
WORKDIR /src

# run a command inside the image (at runtime) in this case to install some dependencies (layer 4)
RUN npm install

# expose the container's posrt 8080 (does not create a layer)
EXPOSE 8080

# tell the container to run node ./app.js on launch (does not create a layer)
ENTRYPOINT ["node", "./app.js"]
```

From the directory in which you have the Dockerfile and your app you can then run:

**Build the image**: `docker image build -t psweb .`

The final parameter is the **build context** this is where you Dockerfile and your source code is. In this instance `.` (the dot) means the current working directory.

Build context can be a remote git repo, for example

`docker image build -t psweb https://github.com/nigelpoulton/psweb.git`

or

`docker build -t psweb https://github.com/nigelpoulton/psweb.git`

**Run the container**: `docker container run -d --name web1 -p 8080:8080 psweb`

### Multistage Builds

Like this ...

```dockerfile
FROM node:latest AS storefront
WORKDIR /usr/src/atsea/app/react-app
COPY react-app .
RUN npm install
RUN npm run build

FROM maven:latest AS appserver
WORKDIR /usr/src/atsea
COPY pom.xml .
RUN mvn -B -f pom.xml -s /usr/share/maven/ref/settings-docker.xml dependency:resolve
COPY . .
RUN mvn -B -s /usr/share/maven/ref/settings-docker.xml package -DskipTests

FROM java:8-jdk-alpine
RUN adduser -Dh /home/gordon gordon
WORKDIR /static
# just take the build from storefront  not the whole factory
COPY --from=storefront /usr/src/atsea/app/react-app/build/ .
WORKDIR /app
# just take the build from appserver  not the whole factory
COPY --from=appserver /usr/src/atsea/target/AtSea-0.0.1-SNAPSHOT.jar .
ENTRYPOINT ["java", "-jar", "/app/AtSea-0.0.1-SNAPSHOT.jar"]
CMD ["--spring.profiles.active=postgres"]
```

## Module 7: Working With Containers

Container is just a thin runtime writable layer added on top of an image.

Container lifecyle
+ pausing a container does not delete it and does not get rid of the data in that container
+ stopping a container does not delete it and does not get rid of the data in that container


### Logging

#### Daemon Logs
Logs from the Docker Engine (daemon).

Most Linux systems use systemd in which case the logs get sent to journald and can be read by

`journalctl -u docker.service`

#### Container Logs
Logs from the App or Container. Docker "hopes" that apps log to `stdout` and `stderr`. Design containers so that the application is running as PID 1 and logging to these standard streams.

Docker also supports logging drivers. There are plugins that integrate container logging with existing logging solutions (e.g. Syslog, Gelf, Splunk, Fluentd ...).

You set the  default logging driver for the system by the `deamon.json` config file. After that any new containers will start using that driver.

Most hosts default to JSON for docker log file format. You can use `docker logs container-name` to read the logs.

Override host default logging settings when starting a container by using `--log-driver` and `--log-opts` settings:

`docker container run --log-driver driver-name --log-opts loging-options -d --name container-name -p host-port:container-port image-name`


## Module 8: Building a Secure Swarm

Docker has two parts
+ Secure cluster - at the heart of what Docker does
+ Orchestration - will probably give way to Kubernetes

### Secure Swarm Cluster

This isa  cluster of Docker Nodes with **Managers** and **Workers**.
There's Mutual TLS where Managers and Workers mutually authenticate each other.
All or the network chatter in encrypted and the **Cluster store** is encrypted.


#### Creating the swarm on one node

```
docker swarm init

```

### Orchestration

## Module 9: Container Networking
Detailed course Pluralsight Docker Networking


---
---


## Docker Command Reference

|Keyword|Object|Command|Paramters|Description|
|---|---|---|---|---|
|`docker`|`image`|`pull ...`||Pulls an image from a registry to the local machine|
|`docker`|`image`|`ls`||List local images|
|`docker`|`image`|`ls`|`--digests`|List local images with hashes|
|`docker`|`image`|`inspect`|`[image name]`|Inspect the image's manifest JSON file|
|`docker`|`image`|`rm`|`[image name]`|Deletes the image from the local machine|
|`docker`|`image`|`build`|`-t psweb .`|Builds the image <BR />`-t tagname` with the image name `tagname` <BR />`dir` using the `dir` directory as the source and location for the Dockerfile.|
|`docker`|`container`|`ls`||List local containers|
|`docker`|`container`|`run`|`-d --name web1 -p 8080:8080 psweb`|Run the container <BR />`-d` detached from the terminal from which the command was issued, <BR />`--name CNAME` with the name CNAME, <BR />`-p HP:CP` mapping the host port HP to the container port CP <BR />`psweb` from the psweb image.|
|`docker`|`container`|`run`|`-it`|Runs the container in interactive mode so that you can login to it. Use `Ctrl+P+Q` to get out of the container's shell and back into the shell of the host machine.|
|`docker`|`container`|`stop`|`[container name]` <br />or <br />`[container id]`|Send the stop signal to the process with PID 1 in the container with id = `container id` or name = `container name`<br /> You can just supply the first few characets of the ID; docker is just looking for uniqueness.|
|`docker`|`container`|`exec`|`-it 60 sh`|Execute a command in the container with '60' in its name or ID, make it interactive, and the command is to run the shell.|
|`docker`||`logs`|`container-name`|Read the log files for the container called container-name.|
|`docker`|`history`|`[image_tag]`||Lists the build history for the image and shows which layers have content.|
|`docker`|`port`|`[container name]`||Returns the port mapping for a the containter. It would output something like <br /> `80/tcp -> 0.0.0.0:80`<br />which means port 80 on the container is mapped to port 80 on all insterface on the host.|
|`docker`|`system`|`info`|||
|`docker`|`swarm`|`init`||Create a Docker Swarm from a node previously running in s single engine mode.|
|`docker`|`swarm`|`join`|`manager cryptographic join token` <BR />or <BR />`worker cryptographic join token`|Join an existing Docker Swarm.|
|`docker`|`node`|`ls`||List the nodes in a swarm|
|`docker`|`swarm`|`join-token`|`manager`|Run on a sepearte node to join the swarm created (as a manager)<br /> Sample output:<br />`docker swarm join -token [token-string] [ip address:port]`|
|`docker`|`swarm`|`join-token`|`worker`|Run on a sepearte node to join the swarm created (as a worker)|
|`docker`|`swarm`|`join-token`|`--rotate worker`|Run on a manager node to to generate a worker token.|
|`docker`|`network`|`ls`||List the networks.|
|`docker`|`volume`|`create`||Create a volume.|
|`docker`|`volume`|`inspect`||Inspect a volume.|
|`docker`|`volume`|`ls`||List volumes.|
|`docker`|`volume`|`rm`||Remove one or more volumes.|
